import React from 'react';
import Form from './Form';

class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: 'http://10.10.4.112:8080/api/games',
      value: null
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    fetch(this.state.url, {
      method: 'POST'
    }).then(response => {
      this.setState({gameId: response.headers.get('location').split('/')[3]}, () => {
        console.log(response);
        console.log(this.state.gameId);
      });
    });
    this.setState({value: <Form/>})
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <button onClick={this.handleChange} type='submit' value='submit'>New Game</button>
        {this.state.value}
      </div>
    );
  }
}

export default Button;