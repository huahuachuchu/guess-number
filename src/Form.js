import React from 'react';
import Fieldset from './Fieldset';
import Result from './Result';
import GoodLuck from './GoodLuck';

class Form extends React.Component{
  constructor(props){
    super(props);
  };
  render() {
    return(
      <form>
        <Fieldset />
        <Result />
        <GoodLuck />
      </form>
    );
  };
}
export default Form;
