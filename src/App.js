import React from 'react';
import Form from './Form';
import Button from './Button';
import Fieldset from './Fieldset';

const App = () => {
  return(
    <div className='App'>
      <Button />
    </div>
  );
};
export default App;