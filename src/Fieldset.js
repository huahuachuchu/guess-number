import React from 'react';

class Fieldset extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      list: ['', '', '', '']
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event, index) {
    this.state.list[index] = event.target.value;
    this.setState({
      list:this.state.list
    });
    console.log(this.state.list);
  }

  render() {
    return(
      <section>
        <fieldset>
          <label>1.<input type='text' value={this.state.list[0]} onChange={(event) => this.handleChange(event, 0)} /></label>
          <label>2.<input type='text' value={this.state.list[1]} onChange={(event) => this.handleChange(event, 1)} /></label>
          <label>3.<input type='text' value={this.state.list[2]} onChange={(event) => this.handleChange(event, 2)} /></label>
          <label>4.<input type='text' value={this.state.list[3]} onChange={(event) => this.handleChange(event, 3)} /></label>
        </fieldset>
      </section>
    );
  }
}
export default Fieldset;