import React from 'react';

class GoodLuck extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      guessAnswer:'',
      guessHistory:[],
      guessResult:{},

    };
    this.guessGame = this.guessGame.bind(this);
  }

  guessGame(){
    const that = this;
    const tempAnswer = this.state.guessAnswer.join('');
    const url = this.state.url + '/' + this.state.gameId;
    fetch(url, {
      method: 'PATCH',
    headers: {
    'Content-Type': 'application/json'
    },
    body: JSON.stringify({'answer': tempAnswer})
  })
  .then(response => response.json())
      .then(json => {
        const {hint, correct} = json;
        that.setState({guessResult: {hint, correct}});
        if (correct === true) {
          this.setState({right: true})
        }
      })
      .then(() => {
        const tempHistory = this.state.guessHistory;
        tempHistory.push(this.state.guessResult);
        console.log(tempHistory);
        this.setState({guessHistory: tempHistory});
      })
  }

  render(){
  return (
    <button onClick={this.guessGame}>GoodLuck</button>
  );

}


}
export default GoodLuck;